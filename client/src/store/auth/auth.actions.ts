import { Action } from "redux";
import { IUser, RegisterStatus } from "@common/api";

export class LogInAction implements Action {
    public static readonly Name = "[Auth] Login";
    readonly type = LogInAction.Name;
    constructor(public email:string, public password:string) {}
}

export class LogInSuccessAction implements Action {
    public static readonly Name = "[Auth] Login Success";
    readonly type = LogInSuccessAction.Name;
    constructor(public user:IUser) {}
}

export class LogInErrorAction implements Action {
    public static readonly Name = "[Auth] Login Error";
    readonly type = LogInErrorAction.Name;
    constructor(public error:any) {}
}

export class RegisterAction implements Action {
    public static readonly Name = "[Auth] Register";
    readonly type = RegisterAction.Name;
    constructor(public request:IUser & {password: string}) {}
}

export class RegisterSuccessAction implements Action {
    public static readonly Name = "[Auth] Register Success";
    readonly type = RegisterSuccessAction.Name;
    constructor(public status:RegisterStatus) {}
}

export class RegisterErrorAction implements Action {
    public static readonly Name = "[Auth] Register Error";
    readonly type = RegisterErrorAction.Name;
    constructor(public status:RegisterStatus, public error:any) {}
}

export type AuthActions = 
    | LogInAction 
    | LogInSuccessAction 
    | RegisterAction 
    | RegisterSuccessAction 
    | LogInErrorAction
    | RegisterErrorAction;