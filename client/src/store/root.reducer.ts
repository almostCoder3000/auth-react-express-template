import { combineReducers } from "redux";
import { authReducer, AuthState } from "./auth/auth.reducers";

export interface RootState {
    auth: AuthState;
}

export const rootReducer = combineReducers<RootState>({
    auth: authReducer,
})