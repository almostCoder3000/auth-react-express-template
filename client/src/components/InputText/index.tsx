import * as React from 'react';

interface Props extends React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
    label: string;
    error?: string;
}

const InputText = React.forwardRef((props: Props, ref) => {
    return (
        <div>
            <label htmlFor={props.id}>{props.label}</label>
            <input type="text" {...(props as any)} ref={ref} />
            {props.error && <span>{props.error}</span>}
        </div>
    );
});

export default InputText;
