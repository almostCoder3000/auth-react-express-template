import * as React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import LoginPage from '../pages/Auth/LoginPage';
import SignUpPage from '../pages/Auth/SignUpPage';
import MainPage from '../pages/MainPage';

const RootRouter: React.FC = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={MainPage} />
                <Route exact path="/login" component={LoginPage} />
                <Route exact path="/signup" component={SignUpPage} />
                <Route component={() => <div>Now Found</div>} />
            </Switch>
        </BrowserRouter>
    );
};

export default RootRouter;
