import { UserRole } from "../enums";

export interface IUser {
    firstName: string;
    lastName: string;
    email: string;
    role: UserRole;
}