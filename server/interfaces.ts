import * as mongoose from 'mongoose';

export interface IBaseUser extends mongoose.Document {
    email: string,
    hashedPassword: string,
    salt: string,
    created: Date,
    lastName: string,
    firstName: string,
    _plainPassword: string,
    checkPassword(password: string): boolean,
}

export interface IBlackList extends mongoose.Document {
    token: string;
}