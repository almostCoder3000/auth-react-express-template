import { UsersController } from "../controllers/users.controller";
import * as express from 'express';
import { AuthController } from "../controllers/auth.controllers";

 

// Переписать все с интерфейсами
export default class UserRoutes {
  public usersController: UsersController;
  public authController: AuthController = new AuthController();

  private _router: express.Router;

  constructor() {
    this.usersController = new UsersController();
    this._router = express.Router();

    this._initRoutes();
  }

  get router(): express.Router {
    return this._router;
  }

  private _initRoutes(): void {
    
    this._router.get('/get', this.authController.authenticateJWT, this.usersController.get_users);
    // login, password, firstName, secondName
    this._router.post('/register', this.usersController.create_user);
    // login, password
    this._router.post('/login', this.authController.auth);
    //
    this._router.post('/logout', this.authController.logout);
  }
}
